/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.eva3fermindonoso;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;


import javax.ws.rs.core.Response;
import root.eva3fermindonoso.dao.UsuariosJpaController;
import root.eva3fermindonoso.dao.exceptions.NonexistentEntityException;
import root.eva3fermindonoso.entity.Usuarios;

/**
 *
 * @author fermindonoso
 */

//*con la anotación Path, estamos declarando nuestro servicio rest.
@Path("usuario")
public class UsuarioRest {
    
    
    //creamos nuestro listar usuario
    @GET
    @Produces(MediaType.APPLICATION_JSON)
   public Response listaUsuarios(){
   
       UsuariosJpaController dao = new UsuariosJpaController();
       
       List<Usuarios> lista= dao.findUsuariosEntities();
       return Response.ok("200").entity(lista).build();
   }
   
   //creamos nuestro crear usuario
   @POST
   @Produces(MediaType.APPLICATION_JSON)
   public Response Crear(Usuarios usuario){
       
       UsuariosJpaController dao = new UsuariosJpaController();
       try {
           dao.create(usuario);
       } catch (Exception ex) {
           Logger.getLogger(UsuarioRest.class.getName()).log(Level.SEVERE, null, ex);
       }
       return Response.ok("200").entity(usuario).build();

   }
   
   @PUT
   @Produces(MediaType.APPLICATION_JSON)
   public Response Actualizar(Usuarios usuario){
   
     UsuariosJpaController dao = new UsuariosJpaController();
    
       try {
           dao.edit(usuario);
       } catch (Exception ex) {
           Logger.getLogger(UsuarioRest.class.getName()).log(Level.SEVERE, null, ex);
       }
       return Response.ok("200").entity(usuario).build();

     
   }
   @DELETE
   @Produces(MediaType.APPLICATION_JSON)
   @Path("/{rut}")
      public Response eliminar(@PathParam("rut") String rut){
        try {
            UsuariosJpaController dao = new UsuariosJpaController();
            dao.destroy(rut);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(UsuarioRest.class.getName()).log(Level.SEVERE, null, ex);
        }

    return Response.ok("usuario eliminado").build();
    
   }
   
      
   
}
